# included into various image & text processing scripts

TILE2BIN="./tile2bin"
ENCODETEXT="./encode-text.pl"
BIN2S="./bin2s.pl"

test -x "$TILE2BIN" || { echo "$TILE2BIN: not executable" >&2; exit 1; }
test -x "$ENCODETEXT" || { echo "$ENCODETEXT: not executable" >&2; exit 1; }
test -x "$BIN2S" || { echo "$BIN2S: not executable" >&2; exit 1; }

label () {
	echo
	echo "$1"
}

# font processing

font8x5 () {
	local sheet="$1"
	local x="$2"
	local y="$3"
	shift 3
	"$TILE2BIN" -x "$x" -y "$y" -w 8 -h 5 "$@" "$sheet" | "$BIN2S"
	echo
}

font8x6w () {
	local sheet="$1"
	local x="$2"
	local y="$3"
	local w="$4"
	shift 4
	echo -e "\t\tfcb $((w+2))"
	"$TILE2BIN" -x "$x" -y "$y" -w 8 -h 6 "$@" "$sheet" | "$BIN2S"
	echo
}

font8x7 () {
	local sheet="$1"
	local x="$2"
	local y="$3"
	shift 3
	"$TILE2BIN" -x "$x" -y "$y" -w 8 -h 7 "$@" "$sheet" | "$BIN2S"
	echo
}

# tile processing

bitmap_a () {
	local sheet="$1"
	local name="$2"
	local x="$3"
	local y="$4"
	shift 4
	label "${name}_a"
	"$TILE2BIN" -x $x -y $y -w $W -h $H "$@" "$sheet" | "$BIN2S"
}

bitmap_b () {
	local sheet="$1"
	local name="$2"
	local x="$3"
	local y="$4"
	shift 4
	label "${name}_b"
	"$TILE2BIN" -s 4 -x $x -y $y -w $W -h $H "$@" "$sheet" | "$BIN2S"
}

bitmap_01_a () {
	local sheet="$1"
	local name="$2"
	local x="$3"
	local y="$4"
	shift 4
	bitmap_a "$sheet" "${name}0" $x $y "$@"
	bitmap_a "$sheet" "${name}1" $((x+W)) $y "$@"
}

# text encoding functions

encode () {
	( cat; echo -ne '\00') | "$ENCODETEXT" | "$BIN2S"
}

encode_file () {
	cat "$1" | perl -pe 'chomp if eof' | encode
}

cursor () {
	echo -e "\t\tfcb $1,$2"
}

encode_string () {
	echo -en "$1" | encode
}

ptr_print_at () {
	local y="$1"
	local x="$2"
	local ptr="$3"
	shift 3
	cursor $((y+1)) $x
	echo -e "\t\tfdb $ptr"
}

ptr_print () {
	local ptr="$1"
	shift 1
	echo -e "\t\tfcb 255"
	echo -e "\t\tfdb $ptr"
}

print_char_at () {
	local y="$1"
	local x="$2"
	local chr="$3"
	shift 3
	cursor $((y+1)) $x
	echo -en "$chr" | "$ENCODETEXT" | "$BIN2S"
}

textlist_done () {
	echo -e "\t\tfcb 0"
}
