; BBC Master Dunjunz song data
;
; Music composed by Julian Avis
;
; This "recording" obtained by modifying Beebem to dump sound register &
; timing information.  Result broken into 960ms (usually) chunks and
; analysed for repeats.

; Song pattern pointers are of the form:
;
;  fdb attenuation1,frequency1  ; channel 1
;  fdb attenuation2,frequency2  ; channel 2
;  fdb attenuation3,frequency3  ; channel 3

; Attenuation patterns are of the form:
;
;  fcb initial_duration+1    ; wait until first "register write"
;  fcb duration+1            ; duration next value applies (255=to end of pattern)
;  fcb value                 ; 8-bit "register value"

; Frequency patterns are similar:
;
;  fcb initial_duration+1    ; wait until first "register write"
;  fcb duration+1            ; duration next value applies (255=to end of pattern)
;  fcb value0,value1,value3  ; 24-bit "register value"

; Song is played from "song" to "song_end", and then repeats from "song_repeat".

song

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_4

song_repeat

	fdb song_pat_6,song_pat_8
	fdb song_pat_7,song_pat_9
	fdb song_pat_1,song_pat_2

	fdb song_pat_10,song_pat_8
	fdb song_pat_11,song_pat_12
	fdb song_pat_3,song_pat_4

	fdb song_pat_13,song_pat_8
	fdb song_pat_14,song_pat_15
	fdb song_pat_5,song_pat_2

	fdb song_pat_16,song_pat_8
	fdb song_pat_17,song_pat_18
	fdb song_pat_3,song_pat_4

	fdb song_pat_19,song_pat_8
	fdb song_pat_20,song_pat_21
	fdb song_pat_1,song_pat_2

	fdb song_pat_22,song_pat_8
	fdb song_pat_23,song_pat_24
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_26,song_pat_27
	fdb song_pat_5,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_28,song_pat_29
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_31
	fdb song_pat_30,song_pat_32
	fdb song_pat_1,song_pat_33

	fdb song_pat_25,song_pat_31
	fdb song_pat_34,song_pat_35
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_31
	fdb song_pat_37,song_pat_38
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_31
	fdb song_pat_39,song_pat_40
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_8
	fdb song_pat_41,song_pat_9
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_34,song_pat_12
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_37,song_pat_15
	fdb song_pat_5,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_39,song_pat_18
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_42,song_pat_21
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_43,song_pat_24
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_44,song_pat_27
	fdb song_pat_5,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_45,song_pat_46
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_30,song_pat_47
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_48,song_pat_49
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_31
	fdb song_pat_50,song_pat_38
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_31
	fdb song_pat_51,song_pat_52
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_8
	fdb song_pat_30,song_pat_47
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_48,song_pat_53
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_31
	fdb song_pat_50,song_pat_54
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_56
	fdb song_pat_55,song_pat_57
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_8
	fdb song_pat_50,song_pat_15
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_58,song_pat_18
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_59,song_pat_21
	fdb song_pat_5,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_60,song_pat_61
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_30,song_pat_47
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_48,song_pat_49
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_31
	fdb song_pat_50,song_pat_38
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_31
	fdb song_pat_51,song_pat_52
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_8
	fdb song_pat_30,song_pat_47
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_48,song_pat_53
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_31
	fdb song_pat_50,song_pat_54
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_56
	fdb song_pat_55,song_pat_57
	fdb song_pat_3,song_pat_36

	fdb song_pat_62,song_pat_63
	fdb song_pat_50,song_pat_15
	fdb song_pat_1,song_pat_2

	fdb song_pat_64,song_pat_65
	fdb song_pat_58,song_pat_18
	fdb song_pat_3,song_pat_4

	fdb song_pat_66,song_pat_67
	fdb song_pat_59,song_pat_21
	fdb song_pat_5,song_pat_2

	fdb song_pat_68,song_pat_70
	fdb song_pat_69,song_pat_24
	fdb song_pat_3,song_pat_4

	fdb song_pat_71,song_pat_73
	fdb song_pat_72,song_pat_27
	fdb song_pat_1,song_pat_2

	fdb song_pat_74,song_pat_63
	fdb song_pat_75,song_pat_76
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_78
	fdb song_pat_77,song_pat_12
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_79,song_pat_15
	fdb song_pat_3,song_pat_4

	fdb song_pat_80,song_pat_82
	fdb song_pat_81,song_pat_83
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_11,song_pat_84
	fdb song_pat_3,song_pat_4

	fdb song_pat_80,song_pat_85
	fdb song_pat_41,song_pat_86
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_34,song_pat_87
	fdb song_pat_3,song_pat_4

	fdb song_pat_80,song_pat_88
	fdb song_pat_41,song_pat_89
	fdb song_pat_1,song_pat_33

	fdb song_pat_0,song_pat_0
	fdb song_pat_34,song_pat_90
	fdb song_pat_3,song_pat_36

	fdb song_pat_80,song_pat_91
	fdb song_pat_41,song_pat_83
	fdb song_pat_5,song_pat_92

	fdb song_pat_0,song_pat_0
	fdb song_pat_34,song_pat_84
	fdb song_pat_3,song_pat_93

	fdb song_pat_7,song_pat_94
	fdb song_pat_41,song_pat_95
	fdb song_pat_1,song_pat_2

	fdb song_pat_11,song_pat_96
	fdb song_pat_34,song_pat_84
	fdb song_pat_3,song_pat_4

	fdb song_pat_41,song_pat_97
	fdb song_pat_41,song_pat_86
	fdb song_pat_5,song_pat_2

	fdb song_pat_34,song_pat_98
	fdb song_pat_34,song_pat_87
	fdb song_pat_3,song_pat_4

	fdb song_pat_41,song_pat_99
	fdb song_pat_41,song_pat_89
	fdb song_pat_1,song_pat_33

	fdb song_pat_34,song_pat_100
	fdb song_pat_34,song_pat_90
	fdb song_pat_3,song_pat_36

	fdb song_pat_41,song_pat_101
	fdb song_pat_41,song_pat_83
	fdb song_pat_5,song_pat_92

	fdb song_pat_34,song_pat_102
	fdb song_pat_34,song_pat_84
	fdb song_pat_3,song_pat_93

	fdb song_pat_37,song_pat_103
	fdb song_pat_37,song_pat_104
	fdb song_pat_1,song_pat_2

	fdb song_pat_39,song_pat_105
	fdb song_pat_39,song_pat_106
	fdb song_pat_3,song_pat_4

	fdb song_pat_42,song_pat_107
	fdb song_pat_42,song_pat_108
	fdb song_pat_5,song_pat_2

	fdb song_pat_43,song_pat_109
	fdb song_pat_43,song_pat_110
	fdb song_pat_3,song_pat_4

	fdb song_pat_44,song_pat_111
	fdb song_pat_44,song_pat_112
	fdb song_pat_1,song_pat_2

	fdb song_pat_113,song_pat_114
	fdb song_pat_113,song_pat_95
	fdb song_pat_3,song_pat_4

	fdb song_pat_115,song_pat_102
	fdb song_pat_115,song_pat_84
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_116
	fdb song_pat_0,song_pat_117
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_118,song_pat_119
	fdb song_pat_1,song_pat_2

	fdb song_pat_120,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_4

	fdb song_pat_123,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_2

	fdb song_pat_126,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_4

	fdb song_pat_130,song_pat_131
	fdb song_pat_48,song_pat_119
	fdb song_pat_1,song_pat_33

	fdb song_pat_132,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_36

	fdb song_pat_133,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_33

	fdb song_pat_134,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_36

	fdb song_pat_135,song_pat_131
	fdb song_pat_48,song_pat_119
	fdb song_pat_1,song_pat_92

	fdb song_pat_136,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_93

	fdb song_pat_137,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_92

	fdb song_pat_137,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_93

	fdb song_pat_135,song_pat_131
	fdb song_pat_48,song_pat_119
	fdb song_pat_1,song_pat_138

	fdb song_pat_136,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_139

	fdb song_pat_137,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_140

	fdb song_pat_137,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_139

	fdb song_pat_135,song_pat_131
	fdb song_pat_48,song_pat_119
	fdb song_pat_1,song_pat_2

	fdb song_pat_136,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_4

	fdb song_pat_137,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_2

	fdb song_pat_137,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_4

	fdb song_pat_135,song_pat_131
	fdb song_pat_48,song_pat_119
	fdb song_pat_1,song_pat_33

	fdb song_pat_136,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_36

	fdb song_pat_137,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_33

	fdb song_pat_137,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_36

	fdb song_pat_135,song_pat_131
	fdb song_pat_48,song_pat_119
	fdb song_pat_1,song_pat_92

	fdb song_pat_136,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_93

	fdb song_pat_137,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_92

	fdb song_pat_137,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_93

	fdb song_pat_135,song_pat_131
	fdb song_pat_48,song_pat_119
	fdb song_pat_1,song_pat_138

	fdb song_pat_136,song_pat_121
	fdb song_pat_0,song_pat_122
	fdb song_pat_3,song_pat_139

	fdb song_pat_137,song_pat_124
	fdb song_pat_0,song_pat_125
	fdb song_pat_5,song_pat_140

	fdb song_pat_137,song_pat_128
	fdb song_pat_127,song_pat_129
	fdb song_pat_3,song_pat_139

	fdb song_pat_135,song_pat_131
	fdb song_pat_48,song_pat_141
	fdb song_pat_1,song_pat_2

	fdb song_pat_142,song_pat_143
	fdb song_pat_0,song_pat_144
	fdb song_pat_3,song_pat_4

	fdb song_pat_145,song_pat_146
	fdb song_pat_0,song_pat_141
	fdb song_pat_5,song_pat_2

	fdb song_pat_147,song_pat_148
	fdb song_pat_0,song_pat_144
	fdb song_pat_3,song_pat_4

	fdb song_pat_149,song_pat_150
	fdb song_pat_0,song_pat_141
	fdb song_pat_1,song_pat_2

	fdb song_pat_151,song_pat_152
	fdb song_pat_0,song_pat_144
	fdb song_pat_3,song_pat_4

	fdb song_pat_153,song_pat_150
	fdb song_pat_0,song_pat_141
	fdb song_pat_5,song_pat_2

	fdb song_pat_154,song_pat_152
	fdb song_pat_0,song_pat_144
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_155
	fdb song_pat_0,song_pat_156
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_157
	fdb song_pat_50,song_pat_158
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_155
	fdb song_pat_48,song_pat_156
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_157
	fdb song_pat_50,song_pat_158
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_155
	fdb song_pat_48,song_pat_159
	fdb song_pat_1,song_pat_33

	fdb song_pat_0,song_pat_157
	fdb song_pat_50,song_pat_54
	fdb song_pat_3,song_pat_36

	fdb song_pat_0,song_pat_155
	fdb song_pat_48,song_pat_159
	fdb song_pat_5,song_pat_33

	fdb song_pat_0,song_pat_157
	fdb song_pat_50,song_pat_54
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_160
	fdb song_pat_48,song_pat_156
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_161
	fdb song_pat_50,song_pat_158
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_160
	fdb song_pat_48,song_pat_156
	fdb song_pat_5,song_pat_2

	fdb song_pat_25,song_pat_161
	fdb song_pat_50,song_pat_158
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_160
	fdb song_pat_48,song_pat_159
	fdb song_pat_1,song_pat_33

	fdb song_pat_25,song_pat_161
	fdb song_pat_50,song_pat_54
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_160
	fdb song_pat_48,song_pat_159
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_161
	fdb song_pat_50,song_pat_54
	fdb song_pat_3,song_pat_36

	fdb song_pat_62,song_pat_162
	fdb song_pat_58,song_pat_163
	fdb song_pat_1,song_pat_2

	fdb song_pat_64,song_pat_164
	fdb song_pat_59,song_pat_165
	fdb song_pat_3,song_pat_4

	fdb song_pat_66,song_pat_166
	fdb song_pat_69,song_pat_167
	fdb song_pat_5,song_pat_2

	fdb song_pat_68,song_pat_168
	fdb song_pat_72,song_pat_169
	fdb song_pat_3,song_pat_4

	fdb song_pat_71,song_pat_170
	fdb song_pat_75,song_pat_171
	fdb song_pat_1,song_pat_2

	fdb song_pat_74,song_pat_162
	fdb song_pat_77,song_pat_129
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_172
	fdb song_pat_79,song_pat_54
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_173,song_pat_163
	fdb song_pat_3,song_pat_4

	fdb song_pat_174,song_pat_8
	fdb song_pat_0,song_pat_175
	fdb song_pat_1,song_pat_2

	fdb song_pat_176,song_pat_8
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_4

	fdb song_pat_177,song_pat_8
	fdb song_pat_0,song_pat_0
	fdb song_pat_5,song_pat_2

	fdb song_pat_178,song_pat_8
	fdb song_pat_179,song_pat_180
	fdb song_pat_3,song_pat_4

	fdb song_pat_181,song_pat_8
	fdb song_pat_30,song_pat_47
	fdb song_pat_1,song_pat_2

	fdb song_pat_182,song_pat_8
	fdb song_pat_48,song_pat_49
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_31
	fdb song_pat_50,song_pat_38
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_31
	fdb song_pat_51,song_pat_52
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_8
	fdb song_pat_30,song_pat_47
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_48,song_pat_53
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_31
	fdb song_pat_50,song_pat_54
	fdb song_pat_5,song_pat_33

	fdb song_pat_25,song_pat_56
	fdb song_pat_55,song_pat_57
	fdb song_pat_3,song_pat_36

	fdb song_pat_25,song_pat_183
	fdb song_pat_50,song_pat_15
	fdb song_pat_1,song_pat_184

	fdb song_pat_25,song_pat_183
	fdb song_pat_58,song_pat_18
	fdb song_pat_3,song_pat_185

	fdb song_pat_25,song_pat_183
	fdb song_pat_59,song_pat_21
	fdb song_pat_5,song_pat_184

	fdb song_pat_25,song_pat_183
	fdb song_pat_60,song_pat_186
	fdb song_pat_3,song_pat_185

	fdb song_pat_25,song_pat_183
	fdb song_pat_30,song_pat_187
	fdb song_pat_1,song_pat_184

	fdb song_pat_25,song_pat_183
	fdb song_pat_48,song_pat_188
	fdb song_pat_3,song_pat_185

	fdb song_pat_25,song_pat_8
	fdb song_pat_50,song_pat_15
	fdb song_pat_5,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_51,song_pat_189
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_30,song_pat_187
	fdb song_pat_1,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_48,song_pat_190
	fdb song_pat_3,song_pat_4

	fdb song_pat_25,song_pat_8
	fdb song_pat_50,song_pat_158
	fdb song_pat_5,song_pat_2

	fdb song_pat_25,song_pat_8
	fdb song_pat_55,song_pat_191
	fdb song_pat_3,song_pat_4

	fdb song_pat_62,song_pat_192
	fdb song_pat_0,song_pat_193
	fdb song_pat_1,song_pat_33

	fdb song_pat_64,song_pat_195
	fdb song_pat_194,song_pat_196
	fdb song_pat_3,song_pat_36

	fdb song_pat_66,song_pat_198
	fdb song_pat_197,song_pat_199
	fdb song_pat_5,song_pat_33

	fdb song_pat_68,song_pat_201
	fdb song_pat_200,song_pat_202
	fdb song_pat_3,song_pat_36

	fdb song_pat_71,song_pat_203
	fdb song_pat_48,song_pat_204
	fdb song_pat_1,song_pat_140

	fdb song_pat_74,song_pat_192
	fdb song_pat_194,song_pat_205
	fdb song_pat_3,song_pat_139

	fdb song_pat_0,song_pat_206
	fdb song_pat_48,song_pat_204
	fdb song_pat_5,song_pat_140

	fdb song_pat_0,song_pat_0
	fdb song_pat_207,song_pat_208
	fdb song_pat_3,song_pat_139

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_209
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_207,song_pat_210
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_193
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_194,song_pat_196
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_48,song_pat_193
	fdb song_pat_1,song_pat_33

	fdb song_pat_0,song_pat_0
	fdb song_pat_194,song_pat_196
	fdb song_pat_3,song_pat_36

	fdb song_pat_0,song_pat_0
	fdb song_pat_197,song_pat_199
	fdb song_pat_5,song_pat_33

	fdb song_pat_0,song_pat_0
	fdb song_pat_200,song_pat_202
	fdb song_pat_3,song_pat_36

	fdb song_pat_0,song_pat_0
	fdb song_pat_48,song_pat_204
	fdb song_pat_1,song_pat_140

	fdb song_pat_0,song_pat_0
	fdb song_pat_194,song_pat_205
	fdb song_pat_3,song_pat_139

	fdb song_pat_0,song_pat_0
	fdb song_pat_48,song_pat_204
	fdb song_pat_5,song_pat_140

	fdb song_pat_0,song_pat_0
	fdb song_pat_207,song_pat_208
	fdb song_pat_3,song_pat_139

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_209
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_207,song_pat_210
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_0,song_pat_193
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_194,song_pat_196
	fdb song_pat_3,song_pat_4

	fdb song_pat_7,song_pat_97
	fdb song_pat_211,song_pat_212
	fdb song_pat_1,song_pat_213

	fdb song_pat_11,song_pat_98
	fdb song_pat_0,song_pat_214
	fdb song_pat_3,song_pat_215

	fdb song_pat_14,song_pat_216
	fdb song_pat_0,song_pat_217
	fdb song_pat_5,song_pat_213

	fdb song_pat_17,song_pat_218
	fdb song_pat_0,song_pat_219
	fdb song_pat_3,song_pat_215

	fdb song_pat_20,song_pat_220
	fdb song_pat_30,song_pat_221
	fdb song_pat_1,song_pat_2

	fdb song_pat_23,song_pat_223
	fdb song_pat_222,song_pat_224
	fdb song_pat_3,song_pat_4

	fdb song_pat_26,song_pat_225
	fdb song_pat_30,song_pat_9
	fdb song_pat_5,song_pat_2

	fdb song_pat_226,song_pat_227
	fdb song_pat_34,song_pat_12
	fdb song_pat_3,song_pat_4

	fdb song_pat_228,song_pat_98
	fdb song_pat_229,song_pat_230
	fdb song_pat_1,song_pat_92

	fdb song_pat_0,song_pat_231
	fdb song_pat_0,song_pat_232
	fdb song_pat_3,song_pat_93

	fdb song_pat_0,song_pat_0
	fdb song_pat_30,song_pat_83
	fdb song_pat_5,song_pat_92

	fdb song_pat_0,song_pat_0
	fdb song_pat_233,song_pat_234
	fdb song_pat_3,song_pat_93

	fdb song_pat_0,song_pat_0
	fdb song_pat_30,song_pat_235
	fdb song_pat_1,song_pat_140

	fdb song_pat_0,song_pat_0
	fdb song_pat_233,song_pat_236
	fdb song_pat_3,song_pat_139

	fdb song_pat_0,song_pat_0
	fdb song_pat_30,song_pat_83
	fdb song_pat_5,song_pat_140

	fdb song_pat_0,song_pat_0
	fdb song_pat_41,song_pat_89
	fdb song_pat_3,song_pat_139

	fdb song_pat_7,song_pat_227
	fdb song_pat_237,song_pat_238
	fdb song_pat_1,song_pat_213

	fdb song_pat_11,song_pat_98
	fdb song_pat_0,song_pat_214
	fdb song_pat_3,song_pat_215

	fdb song_pat_14,song_pat_216
	fdb song_pat_0,song_pat_217
	fdb song_pat_5,song_pat_213

	fdb song_pat_17,song_pat_218
	fdb song_pat_0,song_pat_219
	fdb song_pat_3,song_pat_215

	fdb song_pat_20,song_pat_220
	fdb song_pat_30,song_pat_221
	fdb song_pat_1,song_pat_2

	fdb song_pat_23,song_pat_223
	fdb song_pat_222,song_pat_224
	fdb song_pat_3,song_pat_4

	fdb song_pat_26,song_pat_225
	fdb song_pat_30,song_pat_9
	fdb song_pat_5,song_pat_2

	fdb song_pat_226,song_pat_227
	fdb song_pat_34,song_pat_12
	fdb song_pat_3,song_pat_4

	fdb song_pat_228,song_pat_98
	fdb song_pat_229,song_pat_230
	fdb song_pat_1,song_pat_92

	fdb song_pat_0,song_pat_231
	fdb song_pat_0,song_pat_232
	fdb song_pat_3,song_pat_93

	fdb song_pat_0,song_pat_0
	fdb song_pat_30,song_pat_83
	fdb song_pat_5,song_pat_92

	fdb song_pat_0,song_pat_0
	fdb song_pat_233,song_pat_234
	fdb song_pat_3,song_pat_93

	fdb song_pat_0,song_pat_0
	fdb song_pat_30,song_pat_235
	fdb song_pat_1,song_pat_140

	fdb song_pat_0,song_pat_0
	fdb song_pat_233,song_pat_236
	fdb song_pat_3,song_pat_139

	fdb song_pat_0,song_pat_0
	fdb song_pat_30,song_pat_83
	fdb song_pat_5,song_pat_140

	fdb song_pat_0,song_pat_0
	fdb song_pat_41,song_pat_89
	fdb song_pat_3,song_pat_139

	fdb song_pat_118,song_pat_239
	fdb song_pat_34,song_pat_90
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_240
	fdb song_pat_37,song_pat_241
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_242
	fdb song_pat_39,song_pat_243
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_244
	fdb song_pat_42,song_pat_245
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_246
	fdb song_pat_43,song_pat_247
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_240
	fdb song_pat_44,song_pat_248
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_242
	fdb song_pat_113,song_pat_249
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_244
	fdb song_pat_115,song_pat_90
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_246
	fdb song_pat_0,song_pat_250
	fdb song_pat_1,song_pat_33

	fdb song_pat_0,song_pat_240
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_36

	fdb song_pat_0,song_pat_242
	fdb song_pat_0,song_pat_0
	fdb song_pat_5,song_pat_33

	fdb song_pat_0,song_pat_244
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_36

	fdb song_pat_0,song_pat_246
	fdb song_pat_0,song_pat_0
	fdb song_pat_1,song_pat_92

	fdb song_pat_0,song_pat_240
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_93

	fdb song_pat_0,song_pat_242
	fdb song_pat_0,song_pat_0
	fdb song_pat_5,song_pat_92

	fdb song_pat_0,song_pat_244
	fdb song_pat_0,song_pat_0
	fdb song_pat_3,song_pat_93

	fdb song_pat_251,song_pat_252
	fdb song_pat_118,song_pat_253
	fdb song_pat_1,song_pat_2

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_257
	fdb song_pat_254,song_pat_258
	fdb song_pat_5,song_pat_2

	fdb song_pat_251,song_pat_260
	fdb song_pat_259,song_pat_261
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_252
	fdb song_pat_0,song_pat_253
	fdb song_pat_1,song_pat_2

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_257
	fdb song_pat_50,song_pat_15
	fdb song_pat_5,song_pat_2

	fdb song_pat_251,song_pat_260
	fdb song_pat_58,song_pat_18
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_252
	fdb song_pat_48,song_pat_262
	fdb song_pat_1,song_pat_33

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_36

	fdb song_pat_251,song_pat_257
	fdb song_pat_254,song_pat_258
	fdb song_pat_5,song_pat_33

	fdb song_pat_251,song_pat_260
	fdb song_pat_259,song_pat_261
	fdb song_pat_3,song_pat_36

	fdb song_pat_251,song_pat_252
	fdb song_pat_0,song_pat_253
	fdb song_pat_1,song_pat_92

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_93

	fdb song_pat_251,song_pat_257
	fdb song_pat_50,song_pat_15
	fdb song_pat_5,song_pat_92

	fdb song_pat_251,song_pat_260
	fdb song_pat_58,song_pat_18
	fdb song_pat_3,song_pat_93

	fdb song_pat_251,song_pat_252
	fdb song_pat_48,song_pat_262
	fdb song_pat_1,song_pat_2

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_257
	fdb song_pat_254,song_pat_258
	fdb song_pat_5,song_pat_2

	fdb song_pat_251,song_pat_260
	fdb song_pat_259,song_pat_261
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_252
	fdb song_pat_0,song_pat_253
	fdb song_pat_1,song_pat_2

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_257
	fdb song_pat_50,song_pat_15
	fdb song_pat_5,song_pat_2

	fdb song_pat_251,song_pat_260
	fdb song_pat_58,song_pat_18
	fdb song_pat_3,song_pat_4

	fdb song_pat_251,song_pat_252
	fdb song_pat_48,song_pat_262
	fdb song_pat_1,song_pat_33

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_36

	fdb song_pat_251,song_pat_257
	fdb song_pat_254,song_pat_258
	fdb song_pat_5,song_pat_33

	fdb song_pat_251,song_pat_260
	fdb song_pat_259,song_pat_261
	fdb song_pat_3,song_pat_36

	fdb song_pat_251,song_pat_252
	fdb song_pat_0,song_pat_253
	fdb song_pat_1,song_pat_92

	fdb song_pat_251,song_pat_255
	fdb song_pat_254,song_pat_256
	fdb song_pat_3,song_pat_93

	fdb song_pat_251,song_pat_257
	fdb song_pat_50,song_pat_15
	fdb song_pat_5,song_pat_92

	fdb song_pat_251,song_pat_260
	fdb song_pat_58,song_pat_18
	fdb song_pat_3,song_pat_93

	fdb song_pat_263,song_pat_0
	fdb song_pat_41,song_pat_221
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_41,song_pat_47
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_41,song_pat_9
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_34,song_pat_12
	fdb song_pat_3,song_pat_4

	fdb song_pat_264,song_pat_265
	fdb song_pat_41,song_pat_47
	fdb song_pat_1,song_pat_2

	fdb song_pat_264,song_pat_266
	fdb song_pat_41,song_pat_221
	fdb song_pat_3,song_pat_4

	fdb song_pat_264,song_pat_267
	fdb song_pat_41,song_pat_9
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_34,song_pat_12
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_37,song_pat_15
	fdb song_pat_1,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_39,song_pat_18
	fdb song_pat_3,song_pat_4

	fdb song_pat_0,song_pat_0
	fdb song_pat_42,song_pat_21
	fdb song_pat_5,song_pat_2

	fdb song_pat_0,song_pat_0
	fdb song_pat_43,song_pat_24
	fdb song_pat_3,song_pat_4

song_end

song_pat_0
	fcb $ff

song_pat_1
	fcb $01,$01,$1b,$08,$55,$03,$44,$01
	fcb $1b,$08,$55,$03,$44,$01,$1b,$08
	fcb $55,$03,$44,$01,$1b,$08,$55,$03
	fcb $44,$01,$1b,$08,$55,$05,$44,$02
	fcb $36,$02,$2b,$02,$22,$02,$1b,$02
	fcb $15,$01,$1b,$08,$55,$03,$44,$01
	fcb $1b,$08,$55,$ff,$44

song_pat_2
	fcb $01,$04,$01,$53,$6d,$04,$01,$4d
	fcb $cd,$08,$01,$53,$6d,$04,$01,$4d
	fcb $cd,$08,$01,$53,$6d,$04,$01,$4d
	fcb $cd,$08,$01,$53,$6d,$04,$01,$4d
	fcb $cd,$04,$01,$53,$6d,$04,$02,$a6
	fcb $da,$04,$02,$9f,$50,$08,$02,$a6
	fcb $da,$04,$02,$9f,$50,$04,$02,$a6
	fcb $da,$04,$01,$53,$6d,$04,$01,$4d
	fcb $cd,$08,$01,$53,$6d,$04,$01,$4d
	fcb $cd,$ff,$01,$53,$6d

song_pat_3
	fcb $03,$02,$36,$02,$2b,$02,$22,$02
	fcb $1b,$02,$15,$01,$1b,$08,$55,$03
	fcb $44,$01,$1b,$08,$55,$05,$44,$02
	fcb $36,$02,$2b,$02,$22,$02,$1b,$02
	fcb $15,$01,$1b,$08,$55,$05,$44,$02
	fcb $36,$02,$2b,$02,$22,$02,$1b,$02
	fcb $15,$02,$11,$02,$0d,$02,$0b,$02
	fcb $09,$02,$07,$02,$05,$01,$1b,$08
	fcb $55,$ff,$44

song_pat_4
	fcb $05,$04,$01,$4d,$cd,$08,$01,$53
	fcb $6d,$04,$01,$4d,$cd,$08,$01,$53
	fcb $6d,$04,$01,$4d,$cd,$08,$01,$53
	fcb $6d,$04,$01,$4d,$cd,$04,$01,$53
	fcb $6d,$04,$02,$a6,$da,$04,$02,$9f
	fcb $50,$08,$02,$a6,$da,$04,$02,$9f
	fcb $50,$08,$02,$a6,$da,$04,$02,$9f
	fcb $50,$08,$02,$a6,$da,$04,$02,$9f
	fcb $50,$ff,$02,$a6,$da

song_pat_5
	fcb $01,$01,$1b,$08,$55,$03,$44,$01
	fcb $1b,$08,$55,$03,$44,$01,$1b,$08
	fcb $55,$05,$44,$02,$36,$02,$2b,$02
	fcb $22,$02,$1b,$02,$15,$01,$1b,$08
	fcb $55,$05,$44,$02,$36,$02,$2b,$02
	fcb $22,$02,$1b,$02,$15,$01,$1b,$08
	fcb $55,$03,$44,$01,$1b,$08,$55,$ff
	fcb $44

song_pat_6
	fcb $23,$26,$03,$ff,$04

song_pat_7
	fcb $01,$07,$1b,$38,$55,$ff,$44

song_pat_8
	fcb $01,$05,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$04,$05,$40
	fcb $9d,$05,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$04,$06,$3e
	fcb $5c,$05,$07,$f4,$8f,$05,$07,$d9
	fcb $77,$0a,$07,$f4,$8f,$04,$07,$d9
	fcb $77,$05,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$ff,$06,$3e
	fcb $5c

song_pat_9
	fcb $01,$07,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$ff,$0a,$81
	fcb $3a

song_pat_10
	fcb $10,$26,$05,$27,$07,$ff,$09

song_pat_11
	fcb $18,$38,$36,$ff,$2b

song_pat_12
	fcb $03,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$ff,$0a,$a9,$ba

song_pat_13
	fcb $23,$26,$0b,$ff,$0d

song_pat_14
	fcb $28,$ff,$22

song_pat_15
	fcb $05,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$ff,$0a,$81,$3a

song_pat_16
	fcb $10,$26,$11,$27,$15,$ff,$1b

song_pat_17
	fcb $38,$ff,$15

song_pat_18
	fcb $0e,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$ff,$0a,$a9
	fcb $ba

song_pat_19
	fcb $23,$26,$22,$ff,$2b

song_pat_20
	fcb $10,$38,$11,$ff,$0d

song_pat_21
	fcb $02,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$ff,$0a,$a9
	fcb $ba

song_pat_22
	fcb $10,$26,$36,$27,$44,$ff,$55

song_pat_23
	fcb $20,$38,$0b,$ff,$09

song_pat_24
	fcb $0b,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$ff,$0a,$81,$3a

song_pat_25
	fcb $0b,$18,$44,$18,$55,$18,$44,$ff
	fcb $55

song_pat_26
	fcb $30,$ff,$07

song_pat_27
	fcb $06,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$ff,$0a,$a9,$ba

song_pat_28
	fcb $08,$38,$05,$09,$04,$07,$2b,$ff
	fcb $55

song_pat_29
	fcb $08,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$02,$0a,$81,$3a,$07,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$ff,$0a,$a9
	fcb $ba

song_pat_30
	fcb $39,$ff,$44

song_pat_31
	fcb $01,$05,$04,$bd,$36,$05,$04,$ad
	fcb $2f,$0a,$04,$bd,$36,$04,$04,$ad
	fcb $2f,$05,$05,$f8,$47,$05,$05,$e3
	fcb $f0,$0a,$05,$f8,$47,$04,$05,$e3
	fcb $f0,$05,$07,$18,$2c,$05,$06,$ff
	fcb $0c,$0a,$07,$18,$2c,$04,$06,$ff
	fcb $0c,$05,$05,$f8,$47,$05,$05,$e3
	fcb $f0,$0a,$05,$f8,$47,$ff,$05,$e3
	fcb $f0

song_pat_32
	fcb $01,$07,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$ff,$09,$5d
	fcb $8a

song_pat_33
	fcb $01,$04,$01,$2e,$17,$04,$01,$2b
	fcb $19,$08,$01,$2e,$17,$04,$01,$2b
	fcb $19,$08,$01,$2e,$17,$04,$01,$2b
	fcb $19,$08,$01,$2e,$17,$04,$01,$2b
	fcb $19,$04,$01,$2e,$17,$04,$02,$5c
	fcb $2e,$04,$02,$56,$33,$08,$02,$5c
	fcb $2e,$04,$02,$56,$33,$04,$02,$5c
	fcb $2e,$04,$01,$2e,$17,$04,$01,$2b
	fcb $19,$08,$01,$2e,$17,$04,$01,$2b
	fcb $19,$ff,$01,$2e,$17

song_pat_34
	fcb $11,$38,$36,$ff,$2b

song_pat_35
	fcb $03,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$ff,$09,$7d,$ad

song_pat_36
	fcb $05,$04,$01,$2b,$19,$08,$01,$2e
	fcb $17,$04,$01,$2b,$19,$08,$01,$2e
	fcb $17,$04,$01,$2b,$19,$08,$01,$2e
	fcb $17,$04,$01,$2b,$19,$04,$01,$2e
	fcb $17,$04,$02,$5c,$2e,$04,$02,$56
	fcb $33,$08,$02,$5c,$2e,$04,$02,$56
	fcb $33,$08,$02,$5c,$2e,$04,$02,$56
	fcb $33,$08,$02,$5c,$2e,$04,$02,$56
	fcb $33,$ff,$02,$5c,$2e

song_pat_37
	fcb $21,$38,$22,$ff,$1b

song_pat_38
	fcb $05,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$ff,$09,$5d,$8a

song_pat_39
	fcb $31,$ff,$15

song_pat_40
	fcb $0e,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$ff,$09,$7d
	fcb $ad

song_pat_41
	fcb $01,$38,$55,$ff,$44

song_pat_42
	fcb $09,$38,$11,$ff,$0d

song_pat_43
	fcb $19,$38,$0b,$ff,$09

song_pat_44
	fcb $29,$ff,$07

song_pat_45
	fcb $01,$38,$05,$10,$04,$07,$2b,$ff
	fcb $55

song_pat_46
	fcb $08,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$02,$0a,$81,$3a,$07,$0a,$a9
	fcb $ba,$05,$0a,$81,$3a,$07,$0b,$f5
	fcb $b9,$ff,$0b,$cc,$e6

song_pat_47
	fcb $01,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0e,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0e,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0e,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0e,$0c,$b0,$47,$ff,$0c,$82
	fcb $5e

song_pat_48
	fcb $01,$ff,$55

song_pat_49
	fcb $01,$07,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0a,$0b,$f5,$b9,$07,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0d,$0a,$a9
	fcb $ba,$07,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$ff,$09,$7d,$ad

song_pat_50
	fcb $21,$38,$44,$ff,$36

song_pat_51
	fcb $31,$18,$2b,$ff,$55

song_pat_52
	fcb $0e,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0a,$09,$7d
	fcb $ad,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$0b,$f5,$b9,$ff,$0b,$cc
	fcb $e6

song_pat_53
	fcb $01,$07,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0a,$0b,$f5,$b9,$07,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0e,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0d,$0c,$b0
	fcb $47,$07,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$ff,$0e,$37,$a3

song_pat_54
	fcb $05,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$ff,$0d,$fe,$18

song_pat_55
	fcb $19,$ff,$55

song_pat_56
	fcb $01,$05,$04,$bd,$36,$05,$04,$ad
	fcb $2f,$0a,$04,$bd,$36,$04,$04,$ad
	fcb $2f,$05,$05,$f8,$47,$05,$05,$e3
	fcb $f0,$0a,$05,$f8,$47,$04,$05,$e3
	fcb $f0,$05,$07,$18,$2c,$05,$06,$ff
	fcb $0c,$0a,$07,$18,$2c,$04,$06,$ff
	fcb $0c,$05,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$ff,$05,$40
	fcb $9d

song_pat_57
	fcb $0e,$07,$0d,$fe,$18,$04,$0e,$37
	fcb $a3,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0a,$0c,$b0,$47,$07,$0b,$f5
	fcb $b9,$07,$0b,$cc,$e6,$0a,$0b,$f5
	fcb $b9,$07,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$ff,$0a,$a9,$ba

song_pat_58
	fcb $31,$ff,$2b

song_pat_59
	fcb $09,$38,$22,$ff,$1b

song_pat_60
	fcb $19,$30,$15,$ff,$55

song_pat_61
	fcb $0b,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$14,$0a,$a9
	fcb $ba,$05,$0a,$81,$3a,$07,$0b,$f5
	fcb $b9,$ff,$0b,$cc,$e6

song_pat_62
	fcb $02,$28,$44,$28,$36,$ff,$2b

song_pat_63
	fcb $02,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$ff,$05,$52,$d0

song_pat_64
	fcb $1a,$28,$22,$ff,$1b

song_pat_65
	fcb $06,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$ff,$05,$52
	fcb $d0

song_pat_66
	fcb $0a,$28,$15,$28,$11,$ff,$0d

song_pat_67
	fcb $05,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$ff,$05,$52,$d0

song_pat_68
	fcb $22,$28,$0b,$ff,$09

song_pat_69
	fcb $19,$38,$15,$ff,$11

song_pat_70
	fcb $09,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$ff,$05,$52
	fcb $d0

song_pat_71
	fcb $12,$28,$07,$ff,$05

song_pat_72
	fcb $29,$ff,$0d

song_pat_73
	fcb $03,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$ff,$05,$40,$9d

song_pat_74
	fcb $02,$28,$04,$28,$03,$ff,$00

song_pat_75
	fcb $01,$38,$0b,$ff,$09

song_pat_76
	fcb $08,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$ff,$0a,$81,$3a

song_pat_77
	fcb $11,$38,$07,$ff,$05

song_pat_78
	fcb $06,$05,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$ff,$05,$40,$9d

song_pat_79
	fcb $21,$38,$04,$ff,$03

song_pat_80
	fcb $1f,$0f,$03,$ff,$00

song_pat_81
	fcb $01,$07,$22,$38,$55,$ff,$44

song_pat_82
	fcb $01,$05,$03,$fc,$93,$05,$03,$ee
	fcb $f7,$0a,$03,$fc,$93,$05,$03,$ee
	fcb $f7,$0a,$03,$fc,$93,$05,$03,$ee
	fcb $f7,$0a,$03,$fc,$93,$05,$03,$ee
	fcb $f7,$0a,$03,$fc,$93,$05,$03,$ee
	fcb $f7,$0a,$03,$fc,$93,$ff,$03,$ee
	fcb $f7

song_pat_83
	fcb $01,$07,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$ff,$05,$42
	fcb $9c

song_pat_84
	fcb $03,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$ff,$05,$54,$dd

song_pat_85
	fcb $01,$05,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$05,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$ff,$05,$40
	fcb $9d

song_pat_86
	fcb $01,$07,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$ff,$06,$41
	fcb $2f

song_pat_87
	fcb $03,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$ff,$06,$58,$23

song_pat_88
	fcb $01,$05,$04,$bd,$36,$05,$04,$ad
	fcb $2f,$0a,$04,$bd,$36,$05,$04,$ad
	fcb $2f,$0a,$04,$bd,$36,$05,$04,$ad
	fcb $2f,$0a,$04,$bd,$36,$05,$04,$ad
	fcb $2f,$0a,$04,$bd,$36,$05,$04,$ad
	fcb $2f,$0a,$04,$bd,$36,$ff,$04,$ad
	fcb $2f

song_pat_89
	fcb $01,$07,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$ff,$05,$e6
	fcb $73

song_pat_90
	fcb $03,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$ff,$05,$fa,$dc

song_pat_91
	fcb $01,$05,$04,$3a,$c2,$05,$04,$2b
	fcb $75,$0a,$04,$3a,$c2,$05,$04,$2b
	fcb $75,$0a,$04,$3a,$c2,$05,$04,$2b
	fcb $75,$0a,$04,$3a,$c2,$05,$04,$2b
	fcb $75,$0a,$04,$3a,$c2,$05,$04,$2b
	fcb $75,$0a,$04,$3a,$c2,$ff,$04,$2b
	fcb $75

song_pat_92
	fcb $01,$04,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$08,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$08,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$08,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$04,$01,$0d,$b9,$04,$02,$1b
	fcb $72,$04,$02,$11,$fb,$08,$02,$1b
	fcb $72,$04,$02,$11,$fb,$04,$02,$1b
	fcb $72,$04,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$08,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$ff,$01,$0d,$b9

song_pat_93
	fcb $05,$04,$01,$08,$fe,$08,$01,$0d
	fcb $b9,$04,$01,$08,$fe,$08,$01,$0d
	fcb $b9,$04,$01,$08,$fe,$08,$01,$0d
	fcb $b9,$04,$01,$08,$fe,$04,$01,$0d
	fcb $b9,$04,$02,$1b,$72,$04,$02,$11
	fcb $fb,$08,$02,$1b,$72,$04,$02,$11
	fcb $fb,$08,$02,$1b,$72,$04,$02,$11
	fcb $fb,$08,$02,$1b,$72,$04,$02,$11
	fcb $fb,$ff,$02,$1b,$72

song_pat_94
	fcb $01,$07,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$0e,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$0e,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$0e,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$0e,$03,$fc,$93,$ff,$03,$ee
	fcb $f7

song_pat_95
	fcb $08,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$ff,$05,$42,$9c

song_pat_96
	fcb $03,$0e,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$0e,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$0e,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$0e,$03,$fc,$93,$07,$03,$ee
	fcb $f7,$ff,$03,$fc,$93

song_pat_97
	fcb $01,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$ff,$05,$40
	fcb $9d

song_pat_98
	fcb $03,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$ff,$05,$52,$d0

song_pat_99
	fcb $01,$07,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$0e,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$0e,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$0e,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$0e,$04,$bd,$36,$ff,$04,$ad
	fcb $2f

song_pat_100
	fcb $03,$0e,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$0e,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$0e,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$0e,$04,$bd,$36,$07,$04,$ad
	fcb $2f,$ff,$04,$bd,$36

song_pat_101
	fcb $01,$07,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$ff,$04,$2b
	fcb $75

song_pat_102
	fcb $03,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$ff,$04,$3a,$c2

song_pat_103
	fcb $05,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$ff,$04,$2b,$75

song_pat_104
	fcb $05,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$ff,$05,$42,$9c

song_pat_105
	fcb $0e,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$ff,$04,$3a
	fcb $c2

song_pat_106
	fcb $0e,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$ff,$05,$54
	fcb $dd

song_pat_107
	fcb $02,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$ff,$04,$3a
	fcb $c2

song_pat_108
	fcb $02,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$ff,$05,$54
	fcb $dd

song_pat_109
	fcb $0b,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$ff,$04,$2b,$75

song_pat_110
	fcb $0b,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$ff,$05,$42,$9c

song_pat_111
	fcb $06,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$0e,$04,$3a,$c2,$07,$04,$2b
	fcb $75,$ff,$04,$3a,$c2

song_pat_112
	fcb $06,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$ff,$05,$54,$dd

song_pat_113
	fcb $01,$38,$05,$ff,$04

song_pat_114
	fcb $08,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$ff,$04,$2b,$75

song_pat_115
	fcb $11,$38,$03,$ff,$00

song_pat_116
	fcb $05,$07,$04,$2b,$75,$0e,$04,$3a
	fcb $c2,$ff,$04,$2b,$75

song_pat_117
	fcb $05,$07,$05,$42,$9c,$0e,$05,$54
	fcb $dd,$ff,$05,$42,$9c

song_pat_118
	fcb $01,$07,$1b,$ff,$55

song_pat_119
	fcb $01,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$09,$7d,$ad,$ff,$09,$5d
	fcb $8a

song_pat_120
	fcb $19,$22,$03,$1f,$04,$ff,$05

song_pat_121
	fcb $01,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$02,$0c,$a4,$ad,$05,$0b,$eb
	fcb $69,$05,$0b,$c2,$dd,$02,$0b,$eb
	fcb $69,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$02,$0c,$a4,$ad,$05,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$02,$0a,$a1
	fcb $88,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$02,$0b,$eb,$69,$05,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$02,$0a,$a1
	fcb $88,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$02,$0b,$eb,$69,$05,$09,$77
	fcb $2e,$05,$09,$57,$36,$ff,$09,$77
	fcb $2e

song_pat_122
	fcb $01,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$09,$7d,$ad,$05,$09,$5d
	fcb $8a,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$08,$78,$1c,$05,$08,$59
	fcb $6e,$07,$09,$7d,$ad,$05,$09,$5d
	fcb $8a,$07,$08,$78,$1c,$05,$08,$59
	fcb $6e,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0a,$a9,$ba,$ff,$0a,$81
	fcb $3a

song_pat_123
	fcb $19,$22,$07,$1f,$09,$ff,$0b

song_pat_124
	fcb $01,$05,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$02,$0a,$a1,$88,$05,$09,$77
	fcb $2e,$05,$09,$57,$36,$02,$09,$77
	fcb $2e,$05,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$02,$0a,$a1,$88,$05,$08,$72
	fcb $ef,$05,$08,$54,$66,$02,$08,$72
	fcb $ef,$05,$09,$77,$2e,$05,$09,$57
	fcb $36,$02,$09,$77,$2e,$05,$08,$72
	fcb $ef,$05,$08,$54,$66,$02,$08,$72
	fcb $ef,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$02,$0c,$a4,$ad,$05,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$ff,$0a,$a1
	fcb $88

song_pat_125
	fcb $01,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0e,$37,$a3,$ff,$0d,$fe
	fcb $18

song_pat_126
	fcb $19,$22,$0d,$1f,$11,$ff,$15

song_pat_127
	fcb $2d,$ff,$44

song_pat_128
	fcb $01,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$02,$0c,$a4,$ad,$05,$0b,$eb
	fcb $69,$05,$0b,$c2,$dd,$02,$0b,$eb
	fcb $69,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$02,$0c,$a4,$ad,$05,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$02,$0a,$a1
	fcb $88,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$02,$0b,$eb,$69,$05,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$02,$0a,$a1
	fcb $88,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$02,$0b,$eb,$69,$05,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$ff,$0e,$29
	fcb $14

song_pat_129
	fcb $03,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$ff,$0e,$37,$a3

song_pat_130
	fcb $3b,$ff,$11

song_pat_131
	fcb $09,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$ff,$0e,$29
	fcb $14

song_pat_132
	fcb $1e,$1f,$15,$22,$1b,$ff,$22

song_pat_133
	fcb $1e,$1f,$2b,$22,$36,$ff,$44

song_pat_134
	fcb $1e,$11,$55,$02,$44,$16,$55,$02
	fcb $44,$16,$55,$ff,$44

song_pat_135
	fcb $27,$28,$36,$ff,$2b

song_pat_136
	fcb $0d,$22,$36,$1f,$44,$11,$55,$ff
	fcb $44

song_pat_137
	fcb $01,$16,$55,$02,$44,$16,$55,$02
	fcb $44,$16,$55,$02,$44,$16,$55,$ff
	fcb $44

song_pat_138
	fcb $01,$04,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$08,$01,$0d,$b9,$04,$01,$08
	fcb $fe,$04,$01,$0d,$b9,$04,$00,$fe
	fcb $ee,$04,$00,$fa,$b2,$08,$00,$fe
	fcb $ee,$04,$00,$fa,$b2,$04,$00,$fe
	fcb $ee,$04,$02,$00,$04,$04,$01,$f7
	fcb $7c,$08,$02,$00,$04,$04,$01,$f7
	fcb $7c,$04,$02,$00,$04,$04,$00,$fe
	fcb $ee,$04,$00,$fa,$b2,$08,$00,$fe
	fcb $ee,$04,$00,$fa,$b2,$ff,$00,$fe
	fcb $ee

song_pat_139
	fcb $05,$04,$00,$fa,$b2,$08,$00,$fe
	fcb $ee,$04,$00,$fa,$b2,$08,$00,$fe
	fcb $ee,$04,$00,$fa,$b2,$08,$00,$fe
	fcb $ee,$04,$00,$fa,$b2,$04,$00,$fe
	fcb $ee,$04,$02,$00,$04,$04,$01,$f7
	fcb $7c,$08,$02,$00,$04,$04,$01,$f7
	fcb $7c,$08,$02,$00,$04,$04,$01,$f7
	fcb $7c,$08,$02,$00,$04,$04,$01,$f7
	fcb $7c,$ff,$02,$00,$04

song_pat_140
	fcb $01,$04,$00,$fe,$ee,$04,$00,$fa
	fcb $b2,$08,$00,$fe,$ee,$04,$00,$fa
	fcb $b2,$08,$00,$fe,$ee,$04,$00,$fa
	fcb $b2,$08,$00,$fe,$ee,$04,$00,$fa
	fcb $b2,$04,$00,$fe,$ee,$04,$02,$00
	fcb $04,$04,$01,$f7,$7c,$08,$02,$00
	fcb $04,$04,$01,$f7,$7c,$04,$02,$00
	fcb $04,$04,$00,$fe,$ee,$04,$00,$fa
	fcb $b2,$08,$00,$fe,$ee,$04,$00,$fa
	fcb $b2,$ff,$00,$fe,$ee

song_pat_141
	fcb $01,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0a,$0c,$b0,$47,$07,$0b,$f5
	fcb $b9,$07,$0b,$cc,$e6,$0a,$0b,$f5
	fcb $b9,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0a,$0c,$b0,$47,$07,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$ff,$0a,$a9
	fcb $ba

song_pat_142
	fcb $17,$28,$22,$ff,$1b

song_pat_143
	fcb $03,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$ff,$0d,$ef,$fd

song_pat_144
	fcb $01,$07,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0a,$0b,$f5,$b9,$07,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0a,$0a,$a9
	fcb $ba,$07,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0a,$0b,$f5,$b9,$07,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$ff,$09,$7d
	fcb $ad

song_pat_145
	fcb $07,$28,$15,$28,$11,$ff,$0d

song_pat_146
	fcb $02,$0a,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$ff,$0e,$29,$14

song_pat_147
	fcb $1f,$28,$0b,$ff,$09

song_pat_148
	fcb $06,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$0a,$0e,$29
	fcb $14,$05,$0d,$ef,$fd,$ff,$0e,$29
	fcb $14

song_pat_149
	fcb $19,$27,$0b,$ff,$0d

song_pat_150
	fcb $01,$05,$0f,$df,$fd,$05,$0f,$aa
	fcb $0b,$0a,$0f,$df,$fd,$04,$0f,$aa
	fcb $0b,$05,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$04,$0d,$ef
	fcb $fd,$05,$0f,$df,$fd,$05,$0f,$aa
	fcb $0b,$0a,$0f,$df,$fd,$04,$0f,$aa
	fcb $0b,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$0a,$0c,$a4,$ad,$ff,$0c,$77
	fcb $17

song_pat_151
	fcb $06,$27,$11,$26,$15,$ff,$1b

song_pat_152
	fcb $01,$05,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$04,$0d,$ef
	fcb $fd,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$0a,$0c,$a4,$ad,$04,$0c,$77
	fcb $17,$05,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$04,$0d,$ef
	fcb $fd,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$0a,$0b,$eb,$69,$ff,$0b,$c2
	fcb $dd

song_pat_153
	fcb $19,$27,$22,$ff,$2b

song_pat_154
	fcb $06,$27,$36,$26,$44,$ff,$55

song_pat_155
	fcb $01,$07,$0b,$eb,$69,$07,$0b,$c2
	fcb $dd,$0a,$0b,$eb,$69,$07,$0a,$a1
	fcb $88,$07,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$07,$0b,$eb,$69,$07,$0b,$c2
	fcb $dd,$0a,$0b,$eb,$69,$07,$0c,$a4
	fcb $ad,$07,$0c,$77,$17,$ff,$0c,$a4
	fcb $ad

song_pat_156
	fcb $01,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0e,$37,$a3,$05,$0d,$fe
	fcb $18,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0e,$37,$a3,$05,$0d,$fe
	fcb $18,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0e,$37,$a3,$05,$0d,$fe
	fcb $18,$07,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$ff,$0f,$f2,$4b

song_pat_157
	fcb $01,$07,$0e,$29,$14,$07,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$07,$0c,$a4
	fcb $ad,$07,$0c,$77,$17,$0a,$0c,$a4
	fcb $ad,$07,$0b,$eb,$69,$07,$0b,$c2
	fcb $dd,$0a,$0b,$eb,$69,$07,$0a,$a1
	fcb $88,$07,$0a,$79,$45,$ff,$0a,$a1
	fcb $88

song_pat_158
	fcb $05,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$ff,$0f,$bb,$dd

song_pat_159
	fcb $01,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0c,$b0,$47,$05,$0c,$82
	fcb $5e,$07,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$ff,$0e,$37,$a3

song_pat_160
	fcb $01,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$0a,$0b,$eb,$69,$04,$0b,$c2
	fcb $dd,$05,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$04,$0a,$79
	fcb $45,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$0a,$0b,$eb,$69,$04,$0b,$c2
	fcb $dd,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$0a,$0c,$a4,$ad,$ff,$0c,$77
	fcb $17

song_pat_161
	fcb $01,$05,$0e,$29,$14,$05,$0d,$ef
	fcb $fd,$0a,$0e,$29,$14,$04,$0d,$ef
	fcb $fd,$05,$0c,$a4,$ad,$05,$0c,$77
	fcb $17,$0a,$0c,$a4,$ad,$04,$0c,$77
	fcb $17,$05,$0b,$eb,$69,$05,$0b,$c2
	fcb $dd,$0a,$0b,$eb,$69,$04,$0b,$c2
	fcb $dd,$05,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$ff,$0a,$79
	fcb $45

song_pat_162
	fcb $02,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$ff,$0a,$a1,$88

song_pat_163
	fcb $0e,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$ff,$0e,$37
	fcb $a3

song_pat_164
	fcb $06,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$ff,$0a,$a1
	fcb $88

song_pat_165
	fcb $02,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$ff,$0e,$37
	fcb $a3

song_pat_166
	fcb $05,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$0a,$0a,$a1,$88,$05,$0a,$79
	fcb $45,$ff,$0a,$a1,$88

song_pat_167
	fcb $0b,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$ff,$0d,$fe,$18

song_pat_168
	fcb $09,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$ff,$0a,$a1
	fcb $88

song_pat_169
	fcb $06,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$ff,$0e,$37,$a3

song_pat_170
	fcb $03,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$ff,$0a,$79,$45

song_pat_171
	fcb $08,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$ff,$0d,$fe,$18

song_pat_172
	fcb $06,$05,$0a,$79,$45,$0a,$0a,$a1
	fcb $88,$ff,$0a,$79,$45

song_pat_173
	fcb $31,$ff,$00

song_pat_174
	fcb $1e,$27,$03,$ff,$04

song_pat_175
	fcb $02,$ff,$0d,$fe,$18

song_pat_176
	fcb $0b,$26,$05,$27,$07,$ff,$09

song_pat_177
	fcb $1e,$27,$0b,$ff,$0d

song_pat_178
	fcb $0b,$26,$11,$27,$15,$ff,$1b

song_pat_179
	fcb $49,$07,$1b,$ff,$55

song_pat_180
	fcb $49,$07,$0a,$a9,$ba,$05,$0a,$81
	fcb $3a,$07,$0b,$f5,$b9,$ff,$0b,$cc
	fcb $e6

song_pat_181
	fcb $1e,$27,$22,$ff,$2b

song_pat_182
	fcb $0b,$26,$36,$27,$44,$ff,$55

song_pat_183
	fcb $01,$05,$05,$f8,$47,$05,$05,$e3
	fcb $f0,$0a,$05,$f8,$47,$04,$05,$e3
	fcb $f0,$05,$07,$18,$2c,$05,$06,$ff
	fcb $0c,$0a,$07,$18,$2c,$04,$06,$ff
	fcb $0c,$05,$08,$72,$ef,$05,$08,$54
	fcb $66,$0a,$08,$72,$ef,$04,$08,$54
	fcb $66,$05,$07,$18,$2c,$05,$06,$ff
	fcb $0c,$0a,$07,$18,$2c,$ff,$06,$ff
	fcb $0c

song_pat_184
	fcb $01,$04,$01,$7e,$64,$04,$01,$77
	fcb $44,$08,$01,$7e,$64,$04,$01,$77
	fcb $44,$08,$01,$7e,$64,$04,$01,$77
	fcb $44,$08,$01,$7e,$64,$04,$01,$77
	fcb $44,$04,$01,$7e,$64,$04,$02,$fc
	fcb $c9,$04,$02,$f3,$39,$08,$02,$fc
	fcb $c9,$04,$02,$f3,$39,$04,$02,$fc
	fcb $c9,$04,$01,$7e,$64,$04,$01,$77
	fcb $44,$08,$01,$7e,$64,$04,$01,$77
	fcb $44,$ff,$01,$7e,$64

song_pat_185
	fcb $05,$04,$01,$77,$44,$08,$01,$7e
	fcb $64,$04,$01,$77,$44,$08,$01,$7e
	fcb $64,$04,$01,$77,$44,$08,$01,$7e
	fcb $64,$04,$01,$77,$44,$04,$01,$7e
	fcb $64,$04,$02,$fc,$c9,$04,$02,$f3
	fcb $39,$08,$02,$fc,$c9,$04,$02,$f3
	fcb $39,$08,$02,$fc,$c9,$04,$02,$f3
	fcb $39,$08,$02,$fc,$c9,$04,$02,$f3
	fcb $39,$ff,$02,$fc,$c9

song_pat_186
	fcb $0b,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0d,$0a,$a9
	fcb $ba,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0c,$b0,$47,$ff,$0c,$82
	fcb $5e

song_pat_187
	fcb $01,$07,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$ff,$0d,$fe
	fcb $18

song_pat_188
	fcb $01,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0a,$0c,$b0,$47,$07,$0b,$f5
	fcb $b9,$07,$0b,$cc,$e6,$0e,$0b,$f5
	fcb $b9,$07,$0b,$cc,$e6,$0d,$0b,$f5
	fcb $b9,$07,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$ff,$0a,$a9,$ba

song_pat_189
	fcb $0e,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0a,$0a,$a9
	fcb $ba,$07,$0b,$f5,$b9,$05,$0b,$cc
	fcb $e6,$07,$0c,$b0,$47,$ff,$0c,$82
	fcb $5e

song_pat_190
	fcb $01,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0a,$0c,$b0,$47,$07,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0d,$0e,$37
	fcb $a3,$07,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$ff,$0f,$f2,$4b

song_pat_191
	fcb $0e,$07,$0f,$bb,$dd,$04,$0f,$f2
	fcb $4b,$07,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0a,$0e,$37,$a3,$07,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0a,$0c,$b0
	fcb $47,$07,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$ff,$0a,$a9,$ba

song_pat_192
	fcb $02,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$ff,$06,$55,$3c

song_pat_193
	fcb $01,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0a,$0c,$b0,$47,$07,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0a,$0e,$37
	fcb $a3,$07,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$0e,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$ff,$0f,$f2,$4b

song_pat_194
	fcb $09,$38,$44,$ff,$36

song_pat_195
	fcb $06,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$ff,$06,$55
	fcb $3c

song_pat_196
	fcb $02,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$ff,$0f,$f2
	fcb $4b

song_pat_197
	fcb $19,$38,$2b,$ff,$22

song_pat_198
	fcb $05,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$0a,$06,$55,$3c,$05,$06,$3e
	fcb $5c,$ff,$06,$55,$3c

song_pat_199
	fcb $0b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$ff,$0f,$bb,$dd

song_pat_200
	fcb $29,$ff,$1b

song_pat_201
	fcb $09,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$ff,$06,$55
	fcb $3c

song_pat_202
	fcb $06,$0e,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$0e,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$0e,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$0e,$0f,$f2,$4b,$07,$0f,$bb
	fcb $dd,$ff,$0f,$f2,$4b

song_pat_203
	fcb $03,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$ff,$06,$3e,$5c

song_pat_204
	fcb $01,$07,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0a,$0a,$a9,$ba,$07,$0b,$f5
	fcb $b9,$07,$0b,$cc,$e6,$0a,$0b,$f5
	fcb $b9,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0e,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$ff,$0c,$b0,$47

song_pat_205
	fcb $02,$07,$0c,$82,$5e,$0e,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0e,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0e,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0e,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$ff,$0c,$b0
	fcb $47

song_pat_206
	fcb $06,$05,$06,$3e,$5c,$0a,$06,$55
	fcb $3c,$ff,$06,$3e,$5c

song_pat_207
	fcb $09,$28,$44,$ff,$55

song_pat_208
	fcb $02,$07,$0c,$82,$5e,$0e,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0e,$0c,$b0
	fcb $47,$05,$0c,$82,$5e,$07,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$ff,$0e,$37
	fcb $a3

song_pat_209
	fcb $01,$07,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0a,$0b,$f5,$b9,$07,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0a,$0c,$b0
	fcb $47,$07,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$0e,$0e,$37,$a3,$07,$0d,$fe
	fcb $18,$ff,$0e,$37,$a3

song_pat_210
	fcb $02,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$07,$0d,$fe,$18,$0e,$0e,$37
	fcb $a3,$05,$0d,$fe,$18,$07,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$ff,$0f,$f2
	fcb $4b

song_pat_211
	fcb $19,$18,$2b,$ff,$55

song_pat_212
	fcb $0b,$07,$0f,$bb,$dd,$0e,$0f,$f2
	fcb $4b,$07,$0f,$bb,$dd,$0a,$0f,$f2
	fcb $4b,$07,$06,$58,$23,$07,$06,$41
	fcb $2f,$0e,$06,$58,$23,$07,$06,$41
	fcb $2f,$ff,$06,$58,$23

song_pat_213
	fcb $01,$04,$01,$95,$7d,$04,$01,$8d
	fcb $7d,$08,$01,$95,$7d,$04,$01,$8d
	fcb $7d,$08,$01,$95,$7d,$04,$01,$8d
	fcb $7d,$08,$01,$95,$7d,$04,$01,$8d
	fcb $7d,$04,$01,$95,$7d,$04,$03,$30
	fcb $75,$04,$03,$1a,$f9,$08,$03,$30
	fcb $75,$04,$03,$1a,$f9,$04,$03,$30
	fcb $75,$04,$01,$95,$7d,$04,$01,$8d
	fcb $7d,$08,$01,$95,$7d,$04,$01,$8d
	fcb $7d,$ff,$01,$95,$7d

song_pat_214
	fcb $01,$07,$07,$f9,$26,$07,$07,$dd
	fcb $ef,$0e,$07,$f9,$26,$07,$07,$dd
	fcb $ef,$0d,$07,$f9,$26,$07,$06,$58
	fcb $23,$07,$06,$41,$2f,$0e,$06,$58
	fcb $23,$07,$06,$41,$2f,$ff,$06,$58
	fcb $23

song_pat_215
	fcb $05,$04,$01,$8d,$7d,$08,$01,$95
	fcb $7d,$04,$01,$8d,$7d,$08,$01,$95
	fcb $7d,$04,$01,$8d,$7d,$08,$01,$95
	fcb $7d,$04,$01,$8d,$7d,$04,$01,$95
	fcb $7d,$04,$03,$30,$75,$04,$03,$1a
	fcb $f9,$08,$03,$30,$75,$04,$03,$1a
	fcb $f9,$08,$03,$30,$75,$04,$03,$1a
	fcb $f9,$08,$03,$30,$75,$04,$03,$1a
	fcb $f9,$ff,$03,$30,$75

song_pat_216
	fcb $05,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$ff,$05,$40,$9d

song_pat_217
	fcb $01,$07,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0e,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0d,$09,$7d,$ad,$07,$07,$f9
	fcb $26,$07,$07,$dd,$ef,$0e,$07,$f9
	fcb $26,$07,$07,$dd,$ef,$ff,$07,$f9
	fcb $26

song_pat_218
	fcb $0e,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$ff,$05,$52
	fcb $d0

song_pat_219
	fcb $01,$07,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0e,$0c,$b0,$47,$07,$0c,$82
	fcb $5e,$0d,$0c,$b0,$47,$07,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$ff,$09,$7d
	fcb $ad

song_pat_220
	fcb $02,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$ff,$05,$52
	fcb $d0

song_pat_221
	fcb $01,$07,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0e,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0e,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0e,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0e,$0b,$f5,$b9,$ff,$0b,$cc
	fcb $e6

song_pat_222
	fcb $11,$38,$36,$ff,$55

song_pat_223
	fcb $0b,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$ff,$05,$40,$9d

song_pat_224
	fcb $03,$0e,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0e,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$0e,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$07,$0b,$f5,$b9,$07,$0c,$b0
	fcb $47,$05,$0c,$82,$5e,$07,$0b,$f5
	fcb $b9,$ff,$0b,$cc,$e6

song_pat_225
	fcb $06,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0e,$05,$52,$d0,$07,$05,$40
	fcb $9d,$ff,$05,$52,$d0

song_pat_226
	fcb $08,$38,$05,$ff,$04

song_pat_227
	fcb $08,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$ff,$05,$40,$9d

song_pat_228
	fcb $18,$38,$03,$ff,$00

song_pat_229
	fcb $21,$10,$22,$ff,$55

song_pat_230
	fcb $05,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$02,$0a,$81,$3a,$07,$04,$3c
	fcb $0e,$07,$04,$2c,$b7,$0e,$04,$3c
	fcb $0e,$07,$04,$2c,$b7,$ff,$04,$3c
	fcb $0e

song_pat_231
	fcb $05,$07,$05,$40,$9d,$0e,$05,$52
	fcb $d0,$07,$05,$40,$9d,$ff,$05,$52
	fcb $d0

song_pat_232
	fcb $01,$07,$04,$be,$d7,$07,$04,$ae
	fcb $c5,$0e,$04,$be,$d7,$07,$04,$ae
	fcb $c5,$0d,$04,$be,$d7,$07,$04,$3c
	fcb $0e,$07,$04,$2c,$b7,$0e,$04,$3c
	fcb $0e,$07,$04,$2c,$b7,$ff,$04,$3c
	fcb $0e

song_pat_233
	fcb $11,$08,$36,$ff,$55

song_pat_234
	fcb $03,$0e,$05,$54,$dd,$07,$05,$42
	fcb $9c,$08,$05,$54,$dd,$07,$05,$42
	fcb $9c,$0a,$05,$54,$dd,$07,$04,$be
	fcb $d7,$07,$04,$ae,$c5,$0a,$04,$be
	fcb $d7,$07,$04,$3c,$0e,$07,$04,$2c
	fcb $b7,$ff,$04,$3c,$0e

song_pat_235
	fcb $01,$07,$03,$fd,$b9,$07,$03,$f0
	fcb $16,$0e,$03,$fd,$b9,$07,$03,$f0
	fcb $16,$0e,$03,$fd,$b9,$07,$03,$f0
	fcb $16,$0e,$03,$fd,$b9,$07,$03,$f0
	fcb $16,$0e,$03,$fd,$b9,$ff,$03,$f0
	fcb $16

song_pat_236
	fcb $03,$0e,$03,$fd,$b9,$07,$03,$f0
	fcb $16,$08,$03,$fd,$b9,$07,$03,$f0
	fcb $16,$0a,$03,$fd,$b9,$07,$04,$3c
	fcb $0e,$07,$04,$2c,$b7,$0a,$04,$3c
	fcb $0e,$07,$04,$be,$d7,$07,$04,$ae
	fcb $c5,$ff,$04,$be,$d7

song_pat_237
	fcb $11,$20,$36,$ff,$55

song_pat_238
	fcb $03,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$04,$05,$fa,$dc,$07,$06,$58
	fcb $23,$07,$06,$41,$2f,$0e,$06,$58
	fcb $23,$07,$06,$41,$2f,$ff,$06,$58
	fcb $23

song_pat_239
	fcb $08,$07,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$07,$05,$f8,$47,$07,$05,$e3
	fcb $f0,$0a,$05,$f8,$47,$07,$05,$52
	fcb $d0,$07,$05,$40,$9d,$0a,$05,$52
	fcb $d0,$07,$06,$55,$3c,$07,$06,$3e
	fcb $5c,$ff,$06,$55,$3c

song_pat_240
	fcb $01,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$07,$18
	fcb $2c,$07,$06,$ff,$0c,$0a,$07,$18
	fcb $2c,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$07,$f4
	fcb $8f,$07,$07,$d9,$77,$ff,$07,$f4
	fcb $8f

song_pat_241
	fcb $05,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$ff,$05,$e6,$73

song_pat_242
	fcb $01,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$07,$18
	fcb $2c,$07,$06,$ff,$0c,$0a,$07,$18
	fcb $2c,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$06,$55
	fcb $3c,$07,$06,$3e,$5c,$ff,$06,$55
	fcb $3c

song_pat_243
	fcb $0e,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$ff,$05,$fa
	fcb $dc

song_pat_244
	fcb $01,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$05,$f8
	fcb $47,$07,$05,$e3,$f0,$0a,$05,$f8
	fcb $47,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$04,$bd
	fcb $36,$07,$04,$ad,$2f,$ff,$04,$bd
	fcb $36

song_pat_245
	fcb $02,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$ff,$05,$fa
	fcb $dc

song_pat_246
	fcb $01,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$05,$f8
	fcb $47,$07,$05,$e3,$f0,$0a,$05,$f8
	fcb $47,$07,$05,$52,$d0,$07,$05,$40
	fcb $9d,$0a,$05,$52,$d0,$07,$06,$55
	fcb $3c,$07,$06,$3e,$5c,$ff,$06,$55
	fcb $3c

song_pat_247
	fcb $0b,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$ff,$05,$e6,$73

song_pat_248
	fcb $06,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$0e,$05,$fa,$dc,$07,$05,$e6
	fcb $73,$ff,$05,$fa,$dc

song_pat_249
	fcb $08,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$ff,$05,$e6,$73

song_pat_250
	fcb $05,$07,$05,$e6,$73,$0e,$05,$fa
	fcb $dc,$ff,$05,$e6,$73

song_pat_251
	fcb $01,$01,$1b,$08,$55,$08,$44,$07
	fcb $36,$01,$1b,$08,$55,$08,$44,$07
	fcb $36,$01,$1b,$08,$55,$08,$44,$07
	fcb $36,$01,$1b,$08,$55,$08,$44,$ff
	fcb $36

song_pat_252
	fcb $01,$18,$05,$52,$d0,$18,$05,$f8
	fcb $47,$18,$05,$52,$d0,$ff,$06,$55
	fcb $3c

song_pat_253
	fcb $01,$07,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0e,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$0d,$0a,$a9,$ba,$07,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0a,$09,$7d
	fcb $ad,$07,$0b,$f5,$b9,$07,$0b,$cc
	fcb $e6,$ff,$0b,$f5,$b9

song_pat_254
	fcb $21,$10,$44,$ff,$55

song_pat_255
	fcb $01,$18,$05,$52,$d0,$18,$07,$18
	fcb $2c,$18,$05,$52,$d0,$ff,$07,$f4
	fcb $8f

song_pat_256
	fcb $05,$07,$0b,$cc,$e6,$0e,$0b,$f5
	fcb $b9,$07,$0b,$cc,$e6,$0e,$0b,$f5
	fcb $b9,$02,$0b,$cc,$e6,$07,$0c,$b0
	fcb $47,$07,$0c,$82,$5e,$0a,$0c,$b0
	fcb $47,$07,$0a,$a9,$ba,$07,$0a,$81
	fcb $3a,$ff,$0a,$a9,$ba

song_pat_257
	fcb $01,$18,$05,$52,$d0,$18,$07,$18
	fcb $2c,$18,$05,$52,$d0,$ff,$06,$55
	fcb $3c

song_pat_258
	fcb $05,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$02,$0a,$81,$3a,$07,$07,$f9
	fcb $26,$07,$07,$dd,$ef,$0a,$07,$f9
	fcb $26,$07,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$ff,$09,$7d,$ad

song_pat_259
	fcb $21,$28,$44,$ff,$55

song_pat_260
	fcb $01,$18,$05,$52,$d0,$18,$05,$f8
	fcb $47,$18,$05,$52,$d0,$ff,$04,$bd
	fcb $36

song_pat_261
	fcb $05,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$07,$09,$5d,$8a,$0e,$09,$7d
	fcb $ad,$05,$09,$5d,$8a,$07,$07,$f9
	fcb $26,$07,$07,$dd,$ef,$ff,$07,$f9
	fcb $26

song_pat_262
	fcb $08,$07,$0a,$81,$3a,$0e,$0a,$a9
	fcb $ba,$07,$0a,$81,$3a,$0d,$0a,$a9
	fcb $ba,$07,$09,$7d,$ad,$07,$09,$5d
	fcb $8a,$0a,$09,$7d,$ad,$07,$0b,$f5
	fcb $b9,$07,$0b,$cc,$e6,$ff,$0b,$f5
	fcb $b9

song_pat_263
	fcb $02,$08,$2b,$06,$22,$02,$1b,$02
	fcb $15,$02,$11,$02,$0d,$02,$0b,$02
	fcb $09,$02,$07,$02,$05,$02,$04,$02
	fcb $03,$ff,$00

song_pat_264
	fcb $01,$01,$1b,$08,$55,$08,$44,$08
	fcb $36,$08,$2b,$06,$22,$02,$1b,$02
	fcb $15,$02,$11,$02,$0d,$02,$0b,$02
	fcb $09,$02,$07,$02,$05,$02,$04,$02
	fcb $03,$ff,$00

song_pat_265
	fcb $01,$ff,$06,$55,$3c

song_pat_266
	fcb $01,$ff,$05,$f8,$47

song_pat_267
	fcb $01,$ff,$05,$52,$d0

; 67.3% reuse of attenuation patterns
; 38.4% reuse of frequency patterns

